//Make a request to server for 5 db entries and parse them into an array, then print targetLanguage/nativeLanguage from first element 
//followed by translation when user clicks reveal. When user clicks next print second element of array etc.

cardsArray = "";
cardsLength = "";
var extraSentences = "";
var extraWords = "";
var currIndex = 0;
var audioFileName = "";
var direction = '';
var sentenceInc = 0;
var wordInc = 0;
var practiceLang = '';
var nativeLn = '';
var ansPlace;

var synth = window.speechSynthesis;

//get elements
var cross = document.getElementById("cross");
var tick = document.getElementById("tick");
var word = document.getElementById("word");
var mnemonic = document.getElementById("mnemonic");
var answerBox = document.getElementById("answer");

//Components of example sentence
var prefix = document.getElementById("prefix");
var content = document.getElementById("content");
var suffix = document.getElementById("suffix");

var wholeExample = document.getElementById("exSen");

//Buttons 
var mnemonicButton =  document.getElementById("mnemonicButton");
var sButton = document.getElementById("exampleButton");
var rButton = document.getElementById("reveal");
var lButton = document.getElementById("listen");
var easyButton = document.getElementById("easy");
var okayButton = document.getElementById("okay");
var hardButton = document.getElementById("hard");

//Buttons for multiple choice
var opt1 = document.getElementById("opt1");
var opt2 = document.getElementById("opt2");
var opt3 = document.getElementById("opt3");
var opt4 = document.getElementById("opt4");

//set buttons to default
let defaultOptions = function() {
  opt1.style = "initial";
  opt2.style = "initial";
  opt3.style = "initial";
  opt4.style = "initial";
}

//disable/enable buttons
let disableChoices = function() {
  opt1.disabled = true;
  opt2.disabled = true;
  opt3.disabled = true;
  opt4.disabled = true;
}

let enableChoices = function() {
  opt1.disabled = false;
  opt2.disabled = false;
  opt3.disabled = false;
  opt4.disabled = false;
}

//Function to capitalize strings
function capitalizeFirst(str) {
  return str.charAt(0).toUpperCase() + str.slice(1);
}


function getRandomInt(max) {
  return Math.floor(Math.random() * Math.floor(max));
}


function checkClueAvailable() {
  if (((cardsArray[currIndex].mnemonic) === "Enter a mnemonic or clue here to help you.")) {

    mnemonicButton.style.display = "none";
   }
  else {
    mnemonicButton.style.display = "initial";
  }
}

var randomNum = getRandomInt(2);

console.log("random num is "+randomNum);

chrome.storage.local.get('pDirection', function(pDir){

  direction = pDir.pDirection;

  if (direction === undefined) {
    direction = 'natToTar';
  }
  
  });

document.getElementById("innerBody").style.display = "none";

//set languages to practice to/from
let getPracticeLang = function() {

  return new Promise(function (succeed, fail) {

    //get languages from chrome local storage
    chrome.storage.local.get('targetLang', function(tLng) {

      practiceLang = tLng.targetLang;
      console.log("prac is "+practiceLang);

      if (practiceLang === undefined) {
        console.log("prac lng is undefined");
        practiceLang = "fr";
      }
      else {
        console.log("prac lng is not undefined");
      }

      chrome.storage.local.get('nativeLang', function(nLng) {

        nativeLn = nLng.nativeLang;

        if (nativeLn === undefined) {
          console.log("nat lng is undefined");
          nativeLn = "en";
          succeed();
        }
        else {
          console.log("nat lng is not undefined");
          succeed();
        }

        console.log("nat is "+nativeLn);
        
      })

    })
    


  })

}


//Request to server for setup
var xmlhttp = new XMLHttpRequest();

console.log("practice window opened");


let requestToServer = function(pracLang, ntLang) {

  var pLang = pracLang;
  var nLang = ntLang;

console.log("request sent from practice");
  return new Promise(function(succeed, fail) {

  var url='https://trns-extension.herokuapp.com';
  xmlhttp.open("POST", url);
  var originId = "practice";

  var handleResponse = function (status, response) {

    responseArray = JSON.parse(response);

    cardsArray = responseArray[0];
    extraWords = responseArray[1];
    extraSentences = responseArray[2];


    cardsLength = cardsArray.length;
    if (cardsLength === 0) {
      noCardsToPractice();
    }
    console.log("There are "+cardsLength+" cards to practice");
    succeed();
 }
 var handleStateChange = function () {
    switch (xmlhttp.readyState) {
       case 0 : // UNINITIALIZED
       case 1 : // LOADING
       case 2 : // LOADED
       case 3 : // INTERACTIVE
       break;
       case 4 : // COMPLETED
       handleResponse(xmlhttp.status, xmlhttp.responseText);
       break;
       default: alert("error");
    }
 }

 xmlhttp.onreadystatechange = handleStateChange;
 
  console.log("practice lang is "+practiceLang);
  var reqArray = [originId, pLang, nLang];
  var arrJSON = JSON.stringify(reqArray);

  xmlhttp.send(arrJSON);

  console.log("log xmlhttp response from practice "+xmlhttp.response);
    })

  }

  //Retrieve languages from storage then call server for cards
  getPracticeLang().then(function() {

  console.log("prac is now "+practiceLang);

    requestToServer(practiceLang, nativeLn).then(function() {
      
      document.getElementById("innerBody").style.display = "initial";

      var testTerm = "";
      
      //get first word/sentence to test on
      if (direction === 'natToTar') {
        testTerm = capitalizeFirst(cardsArray[currIndex].targetLng);
      }
      else if (direction === 'tarToNat') {
        testTerm = capitalizeFirst(cardsArray[currIndex].nativeLng);
      }
  
      word.textContent = testTerm
  
    //Randomly select manual or multiple choice to start
    if (randomNum == 0){
      setUpManual();
    }
    
    //Check there are enough extra words for multiple choice
    else if (cardsArray[currIndex].singleWord == true) {
      if (extraWords[0] != 'Not enough'){
        console.log("Enough extra words");
        setUpMC();
      }
      else {
        console.log("Not enough extra words");
        setUpManual();
      }
    }
  
    //Check there are enough extra sentences for multiple choice
    else if (cardsArray[currIndex].singleWord == false) {
      if (extraSentences[0] != 'Not enough'){
        console.log("Enough extra sentences");
        setUpMC();
      }
      else {
        console.log("Not enough extra sentences");
        setUpManual();
      }
    }
     
  
    });

  })

  //set up user interface to test user on word/sentence by entering translation
  let setUpManual = function() {


    rButton.style.display = "initial";
    wholeExample.style.display = "none";

    //Check whether mnemonic/clue is available
    checkClueAvailable();

    var currentCard = cardsArray[currIndex];
    var exampleSentence = currentCard.exampleArrayTargetPrefix[0]

    //check if current card is word or sentence
    if (currentCard.singleWord == false) {

    sButton.style.display = "none";
    answerBox.rows = 4;

    }


    else if (currentCard.singleWord == true) {


    sButton.style.display = "initial";
    answerBox.rows = 2;

    console.log(cardsArray[currIndex].exampleArrayTargetPrefix[0]);

    }

    //check if word has example available
    if (((exampleSentence) === "No example available")||(exampleSentence == undefined)) {

    sButton.style.display = "none";
    }
    
    prefix.textContent = "";
    content.textContent = "";
    content.style.fontStyle = "italic";
    suffix.textContent = "";
    cross.style.display='none';
    tick.style.display='none';
    answerBox.style.display='initial';
    answerBox.value='';
    mnemonic.style.display='none';
    easyButton.style.display='none';
    okayButton.style.display='none';
    hardButton.style.display='none';  
    opt1.style.display='none';
    opt2.style.display='none';
    opt3.style.display='none';
    opt4.style.display='none';

  };


  //set up user interface to test user on word/sentence by multiple choice
  let setUpMC = function() {

    var currentCard = cardsArray[currIndex];
    //Random number for placement of answer
    ansPlace = getRandomInt(4)+1;
    console.log("ansPlace is "+ansPlace);

    rButton.style.display = "none";

    mnemonic.style.display='none';

    wholeExample.style.display = "none";

    //Check whether mnemonic/clue is available
    checkClueAvailable();

    var opt1 = document.getElementById("opt1");
    var opt2= document.getElementById("opt2");
    var opt3 = document.getElementById("opt3");
    var opt4 = document.getElementById("opt4");

    opt1.style.display = "initial";
    opt2.style.display = "initial";
    opt3.style.display = "initial";
    opt4.style.display = "initial";


    if (direction === 'natToTar') {

      //Test if card is word or sentence
      if (currentCard.singleWord == true) {

        sButton.style.display = "initial";

        console.log("0 add wordInc = "+(0+wordInc));
        opt1.value = capitalizeFirst(extraWords[0+wordInc].nativeLng);
        opt2.value = capitalizeFirst(extraWords[1+wordInc].nativeLng);
        opt3.value = capitalizeFirst(extraWords[2+wordInc].nativeLng);
        opt4.value = capitalizeFirst(extraWords[3+wordInc].nativeLng);

        wordInc++;
      }

      else if (currentCard.singleWord == false) {

      sButton.style.display = "none";
  
      opt1.value = extraSentences[0+sentenceInc].nativeLng;
      opt2.value = extraSentences[1+sentenceInc].nativeLng;
      opt3.value = extraSentences[2+sentenceInc].nativeLng;
      opt4.value = extraSentences[3+sentenceInc].nativeLng;

      sentenceInc++;
      }

      document.getElementById("opt"+ansPlace).value = capitalizeFirst(cardsArray[currIndex].nativeLng);

    }
    else if (direction === 'tarToNat') {

      if (currentCard.singleWord == true) {

        sButton.style.display = "initial";

        opt1.value = extraWords[0+wordInc].targetLng;
        opt2.value = extraWords[1+wordInc].targetLng;
        opt3.value = extraWords[2+wordInc].targetLng;
        opt4.value = extraWords[3+wordInc].targetLng;

        wordInc++;
      }

      else if (currentCard.singleWord == false) {

      sButton.style.display = "none";

      opt1.value = extraSentences[0+sentenceInc].targetLng;
      opt2.value = extraSentences[1+sentenceInc].targetLng;
      opt3.value = extraSentences[2+sentenceInc].targetLng;
      opt4.value = extraSentences[3+sentenceInc].targetLng;

      sentenceInc++;
      }

      //set correct answer
      document.getElementById("opt"+ansPlace).value = cardsArray[currIndex].targetLng;
    }

    //check if example sentence available
    if ((cardsArray[currIndex].exampleArrayTargetPrefix[0]) === "No example available") {
      sButton.style.display = "none";
    }
  
    answerBox.style.display = "none";
    prefix.textContent = "";
    content.textContent = "";
    content.style.fontStyle = "italic";
    suffix.textContent = "";
    cross.style.display='none';
    tick.style.display='none';
    easyButton.style.display='none';
    okayButton.style.display='none';
    hardButton.style.display='none';  

  }


  //check users answer
  let choiceCheck = function(buttonPressed) {

    var buttonValue = document.getElementById("opt"+buttonPressed).value;
    var correctAnswer = "";

    if (direction === 'natToTar') {
      correctAnswer = capitalizeFirst(cardsArray[currIndex].nativeLng);
    }

    else if (direction === 'tarToNat') {
      correctAnswer = capitalizeFirst(cardsArray[currIndex].targetLng);
    }
    

    if (buttonValue === correctAnswer) {

      return true;

    }

    else return false;

  }

  opt1.onclick = function() {


    disableChoices();

    if (choiceCheck(1)) {
      tick.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
     
    }
    else {
      cross.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
      
    }

    easyButton.style.display='initial';
    okayButton.style.display='initial';
    hardButton.style.display='initial';

  }

  opt2.onclick = function() {

    disableChoices();

   
    if (choiceCheck(2)) {
      tick.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
      
    }
    else {
      cross.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
     
    }

    easyButton.style.display='initial';
    okayButton.style.display='initial';
    hardButton.style.display='initial';
  }

  opt3.onclick = function() {

    disableChoices();


    if (choiceCheck(3)) {
      tick.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
      
    }
    else {
      cross.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
      
    }

    easyButton.style.display='initial';
    okayButton.style.display='initial';
    hardButton.style.display='initial';
  }

  opt4.onclick = function() {

    disableChoices();


    if (choiceCheck(4)) {
      tick.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
      
    }
    else {
      cross.style.display = 'initial';
      document.getElementById("opt"+ansPlace).style.color = "green";
      document.getElementById("opt"+ansPlace).style.border = "3px solid green";
     
    }

    easyButton.style.display='initial';
    okayButton.style.display='initial';
    hardButton.style.display='initial';
  }


var nButton = document.getElementById("next");

//When user clicks arrow for next card, increment index and set up interface
nButton.onclick = function() {

  enableChoices();
  defaultOptions();

   var currCard = cardsArray[currIndex];
   currIndex = currIndex+1;
   if (currIndex >= cardsLength) {
     endPractice();
   }

   else {
    var testTerm = "";

    if (direction === 'natToTar') {
      testTerm = capitalizeFirst(cardsArray[currIndex].targetLng);
    }
    else if (direction === 'tarToNat') {
      testTerm = capitalizeFirst(cardsArray[currIndex].nativeLng);
    }

  word.textContent = testTerm

   var numNext = getRandomInt(2);

   if (numNext == 0){
    setUpManual();
  }

  else if (cardsArray[currIndex].singleWord == true) {
    if (extraWords[0] != 'Not enough'){
      console.log("Enough extra words");
      setUpMC();
    }
    else {
      console.log("Not enough extra words");
      setUpManual();
    }
  }

  else if (cardsArray[currIndex].singleWord == false) {
    if (extraSentences[0] != 'Not enough'){
      console.log("Enough extra sentences");
      setUpMC();
    }
    else {
      console.log("Not enough extra sentences");
      setUpManual();
    }
  }
   }
   
 
}


//Reveal answer to user test through entering translation, set up interface and check answer
rButton.onclick = function() {

  var wordContent = '';

  if (direction === 'natToTar') {
    //remove full stops
    wordContent = capitalizeFirst(cardsArray[currIndex].nativeLng);
    word.textContent = wordContent;
    wordContent = wordContent.replace(/[\.'\s]/g,'');
    wordContent = wordContent.replace(/\u2019/g, '');
    console.log(wordContent);

  }
  else if (direction === 'tarToNat') {
    wordContent = cardsArray[currIndex].targetLng;
    word.textContent = wordContent;
    wordContent = wordContent.replace(/[\.'\s]/g,'');
    wordContent = wordContent.replace(/\u2019/g, '');
    console.log(wordContent);
  }
  prefix.textContent = "E.g. "+cardsArray[currIndex].exampleArrayTargetPrefix[0];

  if ((cardsArray[currIndex].exampleArrayTargetPrefix[0]) === "No example available") {
    content.style.display = "none";
    prefix.style.display = "none";
    suffix.style.display = "none";
  }

  else if (direction === 'natToTar') {
    content.textContent = cardsArray[currIndex].nativeLng;
    content.style.fontStyle = "italic";
  }
  else if (direction === 'tarToNat') {
    content.textContent = cardsArray[currIndex].nativeLng;;
    content.style.fontStyle = "italic";
  }

  suffix.textContent = cardsArray[currIndex].exampleArrayTargetSuffix[0]
  answerBox.style.display='none';
  easyButton.style.display='initial';
  okayButton.style.display='initial';
  hardButton.style.display='initial';

  //Make buttons for feedback visible


  var userAnswer = answerBox.value;
  userAnswer = userAnswer.replace(/[\.'\s]/g,'');
  userAnswer = userAnswer.replace(/\u2019/g, '');
  console.log(userAnswer);


  //Check user answer
  if (direction === 'natToTar') {

    //Check strings match, returns 0 if they do
    var match = userAnswer.localeCompare(wordContent, undefined, { sensitivity: 'base' });

    if (!match) {

      tick.style.display = "initial";
      }
    
      else {
    
      cross.style.display = "initial";
      }
  }
  else if (direction === 'tarToNat') {

    var match = userAnswer.localeCompare(wordContent, undefined, { sensitivity: 'base' });

    if (!match) {

      tick.style.display = "initial";
      }
    
      else {
    
      cross.style.display = "initial";
      }
  }
 

}


// Execute a function when the user releases a key on the keyboard
answerBox.addEventListener("keyup", function(event) {
  // Number 13 is the "Enter" key on the keyboard
  if (event.keyCode === 13) {
    // Cancel the default action, if needed
    event.preventDefault();
    // Trigger the button element with a click
    rButton.click();
  }
}); 

//Show example sentence for current card
sButton.onclick = function() {

  wholeExample.style.display = "initial";

  mnemonic.style.display = "none";

  prefix.style.display = "initial";

  content.style.display = "initial";

  suffix.style.display = "initial";

  prefix.textContent = "E.g. "+cardsArray[currIndex].exampleArrayTargetPrefix[0];
  content.textContent = "______";
  content.style.fontStyle = "italic";
  suffix.textContent = cardsArray[currIndex].exampleArrayTargetSuffix[0]

}

//Button for showing mnemonic/clue
mnemonicButton.onclick = function() {

  wholeExample.style.display = "initial";

  prefix.style.display = "none";
  content.style.display = "none";
  suffix.style.display = "none";
  mnemonic.style.display = "initial";
  if (cardsArray[currIndex].mnemonic) {
  mnemonic.textContent = "Clue: "+cardsArray[currIndex].mnemonic;
  }
  else mnemonic.textContent = "No clue for this card";
}



//Listen to target language for current card
lButton.onclick = function() {

  var stringToRead = cardsArray[currIndex].nativeLng;

  var lanToSpeak = cardsArray[currIndex].l2;

  //Use Web Speech API
  var l2S;

  console.log("lanToSpeak is "+lanToSpeak);


  switch (lanToSpeak) {
    case 'pl':
      l2S = 'pl-PL';
      speaker = 'PaulinaRUS';
      break;
    case 'fr':
      l2S = 'fr-FR';
      speaker = 'Julie, Apollo';
      break;
    case 'es':
      l2S = 'es-ES';
      speaker = 'Laura, Apollo';
      break;
    case 'de':
      l2S = 'de-DE';
      speaker = 'Hedda';
      break;
    case 'en':
      l2S = 'en-GB';
      speaker = 'Susan, Apollo';
      break;
    case 'it':
      l2S = 'it-IT';
      speaker = 'LuciaRUS';
      break;
    case 'el':
      l2S = 'el-GR';
      speaker = 'Stefanos';
      break;
    case 'bg':
      l2S = 'bg-BG';
      speaker = 'Ivan';
      break;
    case 'da':
      l2S = 'da-DK';
      speaker = 'HelleRUS';
      break;
    case 'nl':
      l2S = 'nl-NL';
      speaker = 'HannaRUS';
      break;
    case "pt":
      l2S = 'pt-BR';
      speaker = 'HeliaRUS';
      break;
    case 'ja':
      l2S = 'ja-JP';
      speaker = 'Ayumi, Apollo';
      break;
    case 'ro':
      l2S = 'ro-RO';
       speaker = 'Andrei';
       break;
    case 'hr':
      l2S = 'hr-HR';
      speaker = 'Matej';
      break;
    case 'cs':
      l2S = 'cs-CZ';
      speaker = 'Jakub';
      break;
    case 'ru':
      l2S = 'ru-RU';
      speaker = 'Irina, Apollo';
      break;
    case 'zh-Hans':
      l2S = 'zh-CN';
      speaker = 'HuihuiRUS';
      break;
    case 'hi':
      l2S = 'hi-IN';
      speaker = 'Kalpana, Apollo';
      break;
    case 'ar':
      l2S = 'ar-EG';
      speaker = 'Hoda';
      break;

  }

  var speakThis = new SpeechSynthesisUtterance(stringToRead);
  speakThis.pitch = 1;
  speakThis.rate = 1;

  var voices = [];

  let loadVoices = function() {


    return new Promise(function(succeed, fail) {

      voices = synth.getVoices();

      id = setInterval(() => {
        if (voices.length !== 0) {
            console.log(voices);
            succeed();
            clearInterval(id);
        }
    }, 10);

    })
    
  }

  loadVoices().then(function() {
    for (i=0; i < voices.length; i++) {
      if (l2S === voices[i].lang) {
        console.log("log for loop "+voices[i].lang)
        speakThis.voice = voices[i];
        break;
      }
    }

    console.log(l2S);
    console.log(voices);
    console.log(speakThis.voice);
    synth.speak(speakThis);
  
  })

}

//Tell server that current card was rated as 'easy'
easyButton.onclick = function () {

  var currentWord = cardsArray[currIndex]._id;
  console.log("word to update is "+currentWord);
  var timesPracticed = cardsArray[currIndex].timesPracticed;

  let requestToServer = function(currentWord) {

    var wordToUpdate = currentWord;

    return new Promise(function(succeed, fail) {

      var url='https://trns-extension.herokuapp.com';
      xmlhttp.open("POST", url);
      var originId = "easyButton";

      var handleResponse = function(status, response){

        succeed();
      }

      var handleStateChange = function() {
        switch (xmlhttp.readyState) {
          case 0 : // UNINITIALIZED
         case 1 : // LOADING
         case 2 : // LOADED
         case 3 : // INTERACTIVE
         break;
         case 4 : // COMPLETED
         handleResponse(xmlhttp.status, xmlhttp.responseText);
         break;
         default: alert("error");
        }
      }

      xmlhttp.onreadystatechange = handleStateChange;

      //Array of originId and current word to update in db
      var reqArray = [originId, wordToUpdate, timesPracticed];
      var arrJSON = JSON.stringify(reqArray);

      xmlhttp.send(arrJSON);

    })


  }

  requestToServer(currentWord).then(function() {

    console.log("word updated");

  });

}

//Tell server that current card was rated as 'okay'
  okayButton.onclick = function () {

  var currentWord = cardsArray[currIndex]._id;
  console.log("word to update is "+currentWord);
  var timesPracticed = cardsArray[currIndex].timesPracticed;

  let requestToServer = function(currentWord) {

    var wordToUpdate = currentWord;

    return new Promise(function(succeed, fail) {

      var url='https://trns-extension.herokuapp.com';
      xmlhttp.open("POST", url);
      var originId = "okayButton";

      var handleResponse = function(status, response){

        succeed();
      }

      var handleStateChange = function() {
        switch (xmlhttp.readyState) {
          case 0 : // UNINITIALIZED
         case 1 : // LOADING
         case 2 : // LOADED
         case 3 : // INTERACTIVE
         break;
         case 4 : // COMPLETED
         handleResponse(xmlhttp.status, xmlhttp.responseText);
         break;
         default: alert("error");
        }
      }

      xmlhttp.onreadystatechange = handleStateChange;

      //Array of originId and current word to update in db
      var reqArray = [originId, wordToUpdate, timesPracticed];
      var arrJSON = JSON.stringify(reqArray);

      xmlhttp.send(arrJSON);

    })


  }

  requestToServer(currentWord).then(function() {

    console.log("word updated");

  });

}

//Tell server that current card was rated as 'hard'
  hardButton.onclick = function () {

  var currentWord = cardsArray[currIndex]._id;
  console.log("word to update is "+currentWord);
  var timesPracticed = cardsArray[currIndex].timesPracticed;

  let requestToServer = function(currentWord) {

    var wordToUpdate = currentWord;

    return new Promise(function(succeed, fail) {

      var url='https://trns-extension.herokuapp.com';
      xmlhttp.open("POST", url);
      var originId = "hardButton";

      var handleResponse = function(status, response){

        succeed();
      }

      var handleStateChange = function() {
        switch (xmlhttp.readyState) {
          case 0 : // UNINITIALIZED
         case 1 : // LOADING
         case 2 : // LOADED
         case 3 : // INTERACTIVE
         break;
         case 4 : // COMPLETED
         handleResponse(xmlhttp.status, xmlhttp.responseText);
         break;
         default: alert("error");
        }
      }

      xmlhttp.onreadystatechange = handleStateChange;

      //Array of originId and current word to update in db
      var reqArray = [originId, wordToUpdate, timesPracticed];
      var arrJSON = JSON.stringify(reqArray);

      xmlhttp.send(arrJSON);

    })


  }

  requestToServer(currentWord).then(function() {

    console.log("word updated");

  });

}

//Tell user that there are no cards in collection or scheduled for practice
let noCardsToPractice = function() {

  word.textContent = "No cards to practice, try adding some new cards";
  mnemonic.style.display = "none";
  opt1.style.display = "none";
  opt2.style.display = "none";
  opt3.style.display = "none";
  opt4.style.display = "none";
  mnemonicButton.style.display = "none";
  wholeExample.style.display = "none";
  prefix.style.display = "none";
  content.style.display = "none";
  suffix.style.display = "none";
  answerBox.style.display='none';
  cross.style.display='none';
  tick.style.display='none';
  easy.style.display='none';
  okay.style.display='none';
  hard.style.display='none';
  lButton.style.display = 'none';
  rButton.style.display = 'none';
  nButton.style.display = 'none';
  sButton.style.display = 'none';

}

//End session when user has completed or skipped through the 5 cards loaded
let endPractice = function() {

  wholeExample.style.display = "none";
  word.textContent = "Finished practice";
  prefix.style.display = "none";
  content.style.display = "none";
  suffix.style.display = "none";
  answerBox.style.display='none';
  cross.style.display='none';
  tick.style.display='none';
  easyButton.style.display='none';
  okayButton.style.display='none';
  hardButton.style.display='none';
  opt1.style.display='none';
  opt2.style.display='none';
  opt3.style.display='none';
  opt4.style.display='none';
  mnemonicButton.style.display='none';
  lButton.style.display = 'none';
  rButton.style.display = 'none';
  nButton.style.display = 'none';
  sButton.style.display = 'none';


}