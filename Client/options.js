//options for selecting languages and practice direction

var tarLangButton = document.getElementById('saveT');
var natLangButton = document.getElementById('saveN');
var natToTarButton = document.getElementById('natToTar');
var tarToNatButton = document.getElementById('tarToNat');
var natElement = document.getElementById('natElement');
var tarElement = document.getElementById('tarElement');
var natLangChoice = document.getElementById('selectionNat');
var tarLangChoice = document.getElementById('selectionTar');
var note = document.getElementById("note");

natLangButton.onclick = function() {

    var language = natLangChoice.value;
    console.log(language)
    note.textContent = "Native language set to "+natLangChoice.value+"!";
    chrome.storage.local.set({'nativeLang': language});

}

tarLangButton.onclick = function() {

    var l2 = tarLangChoice.value;
    console.log(l2)
    note.textContent = "Target language set to "+tarLangChoice.value+"!";
    chrome.storage.local.set({'targetLang': l2});
}

natToTarButton.onclick = function() {

    chrome.storage.local.set({'pDirection': 'natToTar'});
    note.textContent = "Practice direction set as Native to Target!";
}


tarToNatButton.onclick = function() {

    chrome.storage.local.set({'pDirection': 'tarToNat'});
    note.textContent = "Practice direction set as Target to Native!"
}

