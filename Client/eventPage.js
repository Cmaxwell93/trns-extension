//creates new window using string user selects for translation

var contextMenuItem = {
  "id": "translator",
  "title": "Translate",
  "contexts": ["selection"]
}

var initialString = "";
var translatedString = "";
var nativeLng = "";

function getNativeLang() {

  return new Promise(function(succeed,fail){
    chrome.storage.local.get('nativeLang', function(ntLng){
      nativeLng = ntLng.nativeLang;
      console.log("NATIVE LANGUAGE IS "+nativeLng);

      if (nativeLng === undefined) {
        console.log("nat lng is undefined");
        nativeLng = "en";
        succeed();
      }
      else {
        console.log("nat lng is not undefined");
        succeed();
      }
      
    });
  })
  
};


var xmlhttp = new XMLHttpRequest();

chrome.contextMenus.create(contextMenuItem);

chrome.contextMenus.onClicked.addListener(function(clickData){

  if (clickData.menuItemId == "translator" && clickData.selectionText){

    var userSelection = clickData.selectionText;

    let requestToServer = function() {

    return new Promise(function (succeed, fail) {

    var url='https://trns-extension.herokuapp.com';
    xmlhttp.open("POST", url);
    var originId = "eventPage";

    var handleResponse = function (status, response) {
      console.log("The response is "+response);

      var resArray = JSON.parse(response);
      
      var language = resArray[0].lng;
      chrome.storage.local.set({'orgLng': language});
      console.log("original language is "+language);

      translatedString = resArray[0].string;
      initialString = userSelection;
      succeed();
   }
   var handleStateChange = function () {
      switch (xmlhttp.readyState) {
         case 0 : // UNINITIALIZED
         case 1 : // LOADING
         case 2 : // LOADED
         case 3 : // INTERACTIVE
         break;
         case 4 : // COMPLETED
         handleResponse(xmlhttp.status, xmlhttp.responseText);
         break;
         default: alert("error");
      }
   }

   xmlhttp.onreadystatechange = handleStateChange;

    //Array of originId and userSelection for request to server
    getNativeLang().then(function() {
    var reqArray = [originId, userSelection, nativeLng];
    var arrJSON = JSON.stringify(reqArray);

    xmlhttp.send(arrJSON);

    console.log("log xmlhttp response "+xmlhttp.response);
      })
    });
    

    }

    //create translation_popup window after server translates
    requestToServer().then(function() {

    chrome.storage.local.set({'field': translatedString});

    chrome.storage.local.set({'original': initialString});
    
    var popupWidth = Math.round(screen.availWidth/3);
    var popupHeight = Math.round(screen.availHeight/3);

    var createData = {
      "url": "translation_popup.html",
      "type": "popup",
      "top": 5,
      "left": 5,
      "width": popupWidth,
      "height": popupHeight
    };

    chrome.windows.create(createData, function(){});

    })
    

  }

})
