//manual card creation

var nativeLng = '';
var originalLng = '';
var nativeEntry = '';
var nativeField = document.getElementById("natInput");
var targetEntry = '';
var targetField = document.getElementById("tarInput");
var mnemonicField = document.getElementById("mnemonic");

var button = document.getElementById("saveselection");

chrome.storage.local.get('nativeLang', function(ntLng){

    nativeLng = ntLng.nativeLang;
    console.log("native lng is "+nativeLng);
    
    });
  
chrome.storage.local.get('targetLang', function(oLng){
  
    originalLng = oLng.targetLang;
    console.log("native lng is "+targetLng);
      
    });

//Button sends data back to server where it is saved to collection 
button.onclick = function() {

    var mnemonic = mnemonicField.value;
    nativeEntry = nativeField.value;
    targetEntry = targetField.value;

    console.log(mnemonic);

  
    requestToServer(nativeEntry, targetEntry, mnemonic);

    confirm("Card saved!");

    nativeField.value = "";
    targetField.value = "";
    mnemonicField.value = "";
  
  }
  
  //Request to server for button
  var xmlhttp = new XMLHttpRequest();
  
  let requestToServer = function(trns, orgn, mnem) {
  
    var translated = trns;
    var original = orgn;
    var mnemonic = mnem;
  
    return new Promise(function (succeed, fail) {
  
    var url='https://trns-extension.herokuapp.com';
    xmlhttp.open("POST", url);
    var originId = "manual";
  
    var handleResponse = function (status, response) {
      console.log("The response is "+response);
      succeed();
   }
   var handleStateChange = function () {
      switch (xmlhttp.readyState) {
         case 0 : // UNINITIALIZED
         case 1 : // LOADING
         case 2 : // LOADED
         case 3 : // INTERACTIVE
         break;
         case 4 : // COMPLETED
         handleResponse(xmlhttp.status, xmlhttp.responseText);
         break;
         default: alert("error");
      }
   }
  
   xmlhttp.onreadystatechange = handleStateChange;
   
    // xmlhttp.onreadystatechange = function () {
    //   console.log("HELLO");
    //   console.log("I AM EVENT PAGE " + xmlhttp.responseText);
    // };
  
    //Array of originId and userSelection for request to server
    var reqArray = [originId, translated, original, originalLng, nativeLng, mnemonic];
    var arrJSON = JSON.stringify(reqArray);
  
    xmlhttp.send(arrJSON);
  
    console.log("log xmlhttp response "+xmlhttp.response);
      })
  
    }

