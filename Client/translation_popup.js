//pop-up window created by eventPage.js when user selects translate

var translated = '';
var original = '';
var nativeLng = '';
var originalLng = '';


chrome.storage.local.get('field', function(translation){
  
  translated = translation.field;

  document.getElementById("field").textContent = translation.field;
  });

  chrome.storage.local.get('original', function(userInput){

  original = userInput.original;

    document.getElementById("original").textContent = userInput.original;
  });

  chrome.storage.local.get('nativeLang', function(ntLng){

  nativeLng = ntLng.nativeLang;

  console.log("NATIVE LANGUAGE IS "+nativeLng);      
  });

  chrome.storage.local.get('orgLng', function(oLng){

  originalLng = oLng.orgLng;

  });

var button = document.getElementById("saveselection");

//Button sends data back to server where it is saved to collection 
button.onclick = function() {

  var mnemonic = document.getElementById("mnemonic").value;

  requestToServer(translated, original, mnemonic);
  button.value = "Selection saved!";
  button.disabled = "true";
}

//Request to server for button
var xmlhttp = new XMLHttpRequest();

let requestToServer = function(trns, orgn, mnem) {

  var translated = trns;
  var original = orgn;
  var mnemonic = mnem;

  return new Promise(function (succeed, fail) {

  var url='https://trns-extension.herokuapp.com/';
  xmlhttp.open("POST", url);
  var originId = "popup";

  var handleResponse = function (status, response) {
    console.log("The response is "+response);
    succeed();
 }
 var handleStateChange = function () {
    switch (xmlhttp.readyState) {
       case 0 : // UNINITIALIZED
       case 1 : // LOADING
       case 2 : // LOADED
       case 3 : // INTERACTIVE
       break;
       case 4 : // COMPLETED
       handleResponse(xmlhttp.status, xmlhttp.responseText);
       break;
       default: alert("error");
    }
 }

 xmlhttp.onreadystatechange = handleStateChange;
 

  //Array of originId userSelection, mnemonic and languages for request to server
  var reqArray = [originId, translated, original, nativeLng, originalLng, mnemonic];
  var arrJSON = JSON.stringify(reqArray);

  xmlhttp.send(arrJSON);

  console.log("log xmlhttp response "+xmlhttp.response);
    })

  }



