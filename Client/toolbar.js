//toolbar pop-up for selecting practice or create manual card

var pButton = document.getElementById("practice");

pButton.onclick = function() {

  buttonPressed("practice");

}

var mButton = document.getElementById("manualEntry");

mButton.onclick = function() {

  buttonPressed("manual")

}


let buttonPressed = function(whichButton) {


    //Create window for practice mode
    var popupWidth = Math.round(screen.availWidth/2.2);
    var popupHeight = Math.round(screen.availHeight/2.2);

    var createData = {
      "url": whichButton+".html",
      "type": "popup",
      "top": 5,
      "left": 1400,
      "width": popupWidth,
      "height": popupHeight
    };

    chrome.windows.create(createData, function(){});

    console.log("button pressed");

    document.getElementById("stylesheet").href= whichButton+".css"; 

    document.getElementById("header").innerText= whichButton;

    var elem=document.getElementById(whichButton);
    elem.parentNode.removeChild(elem);

    return false;



}