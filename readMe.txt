INSTALLATION AND RUNNING

The program can be installed and run as follows:-

1. Open Google Chrome and enter 'chrome://extensions' in the address bar

2. Toggle 'Developer Mode' in the top right corner to on

3. Select 'Load unpacked' and open 'Code/Client' from the project folder

4. Icon should now appear in the toolbar and 'translation' option appears in right-click menu for highlighted text

5. To access 'practice' or 'manual entry' click on toolbar icon.

6. To access 'options' right-click on toolbar icon and select options from dropdown menu

All other files can be viewed in 'Code/Back-End'.
