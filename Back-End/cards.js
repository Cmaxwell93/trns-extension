const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const Card = new Schema ({
    nativeLng: {type: String, required: true},
    targetLng: {type: String, required: true},
    nativeDef: {type: String, required: false},
    targetDef: {type: String, required: false},
    l1: {type: String, required: false},
    l2: {type: String, required: false},
    exampleArrayTargetPrefix: {type: Array, required: false},
    exampleArrayTargetSuffix: {type: Array, required: false},
    exampleArrayNativePrefix: {type: Array, required: false},
    exampleArrayNativeSuffix: {type: Array, required: false},
    exSentence: {type: String, required: false},
    mnemonic: {type: String, required: false},
    picture: {type: String, required: false},
    lowPriority: {type: Boolean, required: false},
    medPriority: {type: Boolean, required: false},
    highPriority: {type: Boolean, required: false},
    singleWord: {type: Boolean, required: false},
    nextRep: {type: Date, required: false},
    timesPracticed: {type: Number, required: false}
});

Card.static('findByNativeLng', function(nativeLng) {
    return this.find({ nativeLng });
  });

module.exports = mongoose.model('Card', Card);