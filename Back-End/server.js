//Import required modules
const db = require('./db');
const Card = require('./cards');
const mongoose = require('mongoose');
var MongoClient = require('mongodb').MongoClient;
var dbUrl = "mongodb+srv://calum93:czfZN8FsjEVfoNbv@trnsstorage-2iueo.mongodb.net/myAtlasDb?retryWrites=true&w=majority"

const request = require('request');
const uuidv4 = require('uuid/v4');

// Requires request and request-promise for HTTP requests
// e.g. npm install request request-promise
const rp = require('request-promise');

// Requires readline-sync to read command line inputs
const readline = require('readline-sync');

// Requires xmlbuilder to build the SSML body
const xmlbuilder = require('xmlbuilder');

var http = require('http');
var fs = require('fs');

//Variables for String to be translated and result
var toTranslate = '';
var translatedText = 'TRANSLATED TEXT ';

//Language to translate to
var translateInto = "";

//Create Server
var server = http.createServer(function(req, res){

  var requestString = '';
  var originId = '';
    
  req.on("data", function (data) {

    var reqArray = JSON.parse(data);
    originId = reqArray[0];

    //Perform translation
    if (originId === "eventPage") {

      requestString += reqArray[1];
      translateInto = reqArray[2];
      console.log("log request string "+requestString);
      toTranslate = requestString;

    
      performTranslation(toTranslate, translateInto).then(function() {
     
          var resArray = [{string: translatedText, lng: originalLang}];
          var arrJSON = JSON.stringify(resArray);

            res.setHeader("Access-Control-Allow-Origin", "*");
            res.writeHead(200, {'Content-Type': 'text/plain'});
            res.write(arrJSON);
            res.end();
      }
            )
    }

    //Save created card to db
    else if (originId === "popup") {

              var originalToSave = reqArray[1];
              var translatedToSave = reqArray[2];
              var nativeLng = reqArray[3];
              var originalLng = reqArray[4];
              var mnemonic = reqArray[5];

                retExam(originalToSave, originalLng, nativeLng).then(function(promiseArray) {

                  var prefixArrayTar = promiseArray[0];
                  var suffixArrayTar = promiseArray[1];
                  var prefixArrayNat = promiseArray[2];
                  var suffixArrayNat = promiseArray[3];

                  console.log("prefix tar after retExam is"+prefixArrayTar);
                  console.log("suffix tar after retExam is"+suffixArrayTar);

                  storeToCollection(originalToSave, translatedToSave, prefixArrayTar, suffixArrayTar, prefixArrayNat, suffixArrayNat, nativeLng, originalLng, mnemonic);
  
                  res.setHeader("Access-Control-Allow-Origin", "*");
                  res.writeHead(200, {'Content-Type': 'text/plain'});
                  res.write("Card saved");
                  res.end();
                })

    }

    //Save manually created card
    else if (originId === "manual") {

      var originalToSave = reqArray[1];
      var translatedToSave = reqArray[2];
      var nativeLng = reqArray[3];
      var originalLng = reqArray[4];
      var mnemonic = reqArray[5];

      toTranslate = translatedToSave;

      retExam(originalToSave, originalLng, nativeLng).then(function(promiseArray) {

        var prefixArrayTar = promiseArray[0];
        var suffixArrayTar = promiseArray[1];
        var prefixArrayNat = promiseArray[2];
        var suffixArrayNat = promiseArray[3];

        console.log("prefix tar after retExam is"+prefixArrayTar);
        console.log("suffix tar after retExam is"+suffixArrayTar);

        storeToCollection(translatedToSave, originalToSave, prefixArrayNat, suffixArrayNat, prefixArrayTar, suffixArrayTar, originalLng, nativeLng, mnemonic);

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write("Card saved");
        res.end();
      })
}

    //Start practice
    else if (originId === "practice") {

              var date = new Date();
              var pracLang = reqArray[1];
              var natLang = reqArray[2];
              console.log("practice lang is "+pracLang);
              console.log("Retrieving cards for practice");

              pullCollection(date, pracLang, natLang).then(function(result) {
                console.log("Pull collection callback");
                
                var dbResult = JSON.stringify(result);

                console.log("this is db result "+dbResult);
                
                res.setHeader("Access-Control-Allow-Origin", "*");
                res.writeHead(200, {'Content-Type': 'text/plain'});
                res.write(dbResult);
                res.end();
              })

    }
    
    //Store current practice card as 'easy'
    else if (originId === "easyButton") {

      var wordToStore = reqArray[1];
      console.log("word to update is "+wordToStore);
      var timesPracticed = reqArray[2];

      storePracticeResults(wordToStore, "lowPriority", timesPracticed).then(function() {

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write("Updated as easy");
        res.end();

      });


    }

    //Store current practice card as 'okay'
    else if (originId === "okayButton") {

      var wordToStore = reqArray[1];
      console.log("word to update is "+wordToStore);
      var timesPracticed = reqArray[2];

      storePracticeResults(wordToStore, "medPriority", timesPracticed).then(function() {

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write("Updated as easy");
        res.end();

      });


    }


    //Store current practice card as 'hard'
    else if (originId === "hardButton") {

      var wordToStore = reqArray[1];
      console.log("word to update is "+wordToStore);
      console.log("word to store is "+wordToStore);
      var timesPracticed = reqArray[2];

      storePracticeResults(wordToStore, "highPriority", timesPracticed).then(function() {

        res.setHeader("Access-Control-Allow-Origin", "*");
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.write("Updated as easy");
        res.end();

      });


    }
  

  },);

});


//Start server
var port = process.env.PORT || 8080;
server.listen(port);


let performTranslation = function (toTranslate, translateTo) {

  return new Promise(function(succeed, fail)
  {

    var string = toTranslate;

    //Language user sets translations to
    userLang = translateTo;

    var subscriptionKey = "d6d24f0ef681408ba43eea4dbaef1f5b";

    var endpoint = "https://api.cognitive.microsofttranslator.com/";

    //Configure the request
    let options = {
        method: 'POST',
        baseUrl: endpoint,
        url: 'translate',
        qs: {
          'api-version': '3.0',
          'to': [userLang]
        },
        headers: {
          'Ocp-Apim-Subscription-Key': subscriptionKey,
          'Content-type': 'application/json',
          'X-ClientTraceId': uuidv4().toString()
        },
        body: [{
              'text': string
        }],
        json: true,
    };

    //Make the request to Microsoft Translator API
    request(options, function(err, res, body){

        var newString = body[0].translations[0].text;
        originalLang = body[0].detectedLanguage.language;
        console.log("log translated string "+newString);
        console.log("String translated into "+userLang);
        console.log("String translated from "+originalLang);
        translatedText = newString;
        succeed();

  });



  }) 


};


//Retrieve example sentence from Microsoft API
let retExam = function(translatedText, originalLang, userLang) {

  return new Promise(function(succeed, fail)
  {
    var translation = translatedText;

    var text = toTranslate;

    var subscriptionKey = "d6d24f0ef681408ba43eea4dbaef1f5b";

    var endpoint = "https://api.cognitive.microsofttranslator.com/";

    var prefixArrayTar = [];
    var suffixArrayTar = [];
    var prefixArrayNat = [];
    var suffixArrayNat = [];

    console.log("source language is "+userLang+" and other is "+originalLang);

    //Configure the request
    let options = {
      method: 'POST',
      baseUrl: endpoint,
      url: 'dictionary/examples',
      qs: {
        'api-version':'3.0',
        'from': userLang,
        'to': originalLang
      },
      headers: {
        'Ocp-Apim-Subscription-Key': subscriptionKey,
        'Content-type': 'application/json',
        'X-ClientTraceId': uuidv4().toString()
      },
      body: [{
        'text': translation,
        'translation': text
    }],
    json: true,
      };

    request(options, function(err, res, body){
        console.log('Examples result is: '+JSON.stringify(body, null, 4));

        //Check example sentence exists
       var examplesArray = body[0].examples[0];

        if (examplesArray === undefined) {
          prefixArrayTar[0] = "No example available";
          succeed(prefixArrayTar, suffixArrayTar, prefixArrayNat, suffixArrayNat);
        }
        else {
          var prefix = body[0].examples[0].targetPrefix;
          var term = body[0].examples[0].targetTerm;
          var suffix = body[0].examples[0].targetSuffix;
          console.log("example sentences: "+prefix+" "+term+" "+suffix);

        var nOfExamples = body[0].examples.length;

          prefixArrayTar[0] = body[0].examples[0].targetPrefix;
          suffixArrayTar[0] = body[0].examples[0].targetSuffix;
          prefixArrayNat[0] = body[0].examples[0].sourcePrefix;
          suffixArrayNat[0] = body[0].examples[0].sourceSuffix;

          console.log("prefix tar inside retExam is "+prefixArrayTar);
          console.log("suffix tar inside retExam is "+suffixArrayTar);

          succeedArray = [prefixArrayTar, suffixArrayTar, prefixArrayNat, suffixArrayNat]
      
          succeed(succeedArray);
        }

    });
    

  })

}


//Store card to collection
let storeToCollection = function(original, translated, prefixArrayTar, suffixArrayTar, prefixArrayNat, suffixArrayNat, l1, l2, mnem) {

    console.log("prefix tar at storeCollection is "+prefixArrayTar)
    console.log("suffix tar at storeToCollection is "+suffixArrayTar);
    //Test whether string to store is a word or sentence
    var wordOrSen = Boolean;

      if (/\s/.test(translated)) {
        //if string contains spaces the string is a sentence
        wordOrSen = false;
    }
    else {
      wordOrSen = true;
    }
    


    var date = new Date();

    //Code for writing toTranslate and translatedText to mongoDB
    const card = new Card ({
      targetLng: original,
      nativeLng: translated,
      //l1 = native language of learner, take from options. l2 = language to learn, detect with ms API
      l1: l1,
      l2: l2,
      exampleArrayTargetPrefix: prefixArrayTar,
      exampleArrayTargetSuffix: suffixArrayTar,
      exampleArrayNativePrefix: prefixArrayNat,
      exampleArrayNativeSuffix: suffixArrayNat,
      nextRep: date,
      singleWord: wordOrSen,
      timesPracticed: 0,
      mnemonic: mnem
  });
  card.save().then(result => {
    console.log("log result "+result+"\n");

  })
  .catch(err => console.log("log error "+err));

}

let pullCollection = function(date, pLang, nLang) {

  var leastPracticedVal = "";
  var pracLang = pLang;
  var natLang = nLang;
  
  return new Promise(function(succeed, fail)
  {


    let firstPull = function() {
      
      return new Promise(function(fSucceed, fail) {
        MongoClient.connect(dbUrl, function(err, db) {
          if (err) throw err;
      
          var dbo = db.db("myAtlasDb");
      
          var arrayForLowestPracticed = [];
      
          //find value of least practiced cards
          dbo.collection("cards").aggregate([ 
            { "$group": { 
                "_id": "$timesPracticed", 
                "docs": { "$push": "$$ROOT" } 
            }},
            { "$sort": { "_id": 1 } }, 
            { "$limit": 1 } 
        ]).toArray(function (err, result) {
          if (err) {
            console.log("Error: "+err);
          };
          arrayForLowestPracticed = result;

          leastPracticedVal = arrayForLowestPracticed[0].docs[0].timesPracticed;
         
          //pull least practiced cards due for repetition
          dbo.collection("cards").find({nextRep: {$lte: date}, l1:{$eq: natLang}, l2: {$eq: pracLang} , timesPracticed: {$eq: leastPracticedVal}}).limit(5).toArray(function(err, result) {
            if (err) throw err;    
          console.log("RETURN ALL TEXT FROM DB" + result +"\n\n");
            db.close();
            fSucceed(result);

          });
      
        });
      
        }); 
      })

    }

  

  // //Extra computation for multiple choice mode
  firstPull().then(function(result) {
      var numWords = 0;
      var numSent = 0;

      var practiceArray = result;
      var idArray = [];
      var arrayLength = practiceArray.length;

      for (i=0; i < arrayLength; i++) {

        idArray[i] = practiceArray[i]._id;
        console.log("id is "+idArray[i]);

        if (practiceArray[i].singleWord == true) {
          numWords++;
          console.log("Number of words pulled = "+numWords);
          
        }
        else if (practiceArray[i].singleWord == false){
          numSent++;
          console.log("Number of sentences pulled = "+numSent);
          
        }
      }

      //pull extra cards to populate multiple choice options
      let secondPull = function() {

        return new Promise(function(succeed, fail) {

          var wordsLimit = numWords*4;
          console.log("words limit is "+wordsLimit);
          var sentenceLimit = numSent*4;
          var extraWordsArray = [];
          var extraSentencesArray = [];


          let callDb = function() {

            return new Promise(function(succeed, fail) {

              MongoClient.connect(dbUrl, function(err, db) {
                if (err) throw err;
            
                var dbo = db.db("myAtlasDb");
            
                //Pull cards for extra words
                dbo.collection("cards").find({singleWord: {$eq: true}, _id: {$nin: idArray}, l1:{$eq: natLang}, l2: {$eq: pracLang}}).limit(wordsLimit).toArray(function(err, result) {
                  if (err) throw err;    
                
                if (result.length == wordsLimit) {
                  extraWordsArray = result;
                  console.log("populating exWords array");

                }
                else extraWordsArray[0] = "Not enough";
            
              });
    
                //Pull cards for extra sentences
                dbo.collection("cards").find({singleWord: {$eq: false}, _id: {$nin: idArray}, l1:{$eq: natLang}, l2: {$eq: pracLang}}).limit(sentenceLimit).toArray(function(err, result) {
                  if (err) throw err;    
        
                  if (result.length == sentenceLimit) {
                    extraSentencesArray = result;
                    console.log("populating exSentences array");
                    // console.log("first element is "+extraSentencesArray[0].nativeLng);
                  }
                  else extraSentencesArray[0] = "Not enough";

                  db.close();
                  //succeed for callDb
                  succeed();
                  
    
            });
                         
              }); 

            })

          }


          callDb().then(function() {
            console.log("sending words and sentences array");

            //succeed for second pull
            console.log(extraWordsArray[0]);

            callDBArray = [extraWordsArray, extraSentencesArray];

            //succeed for secondPull
            succeed(callDBArray);
          })


        })

      }

      secondPull().then(function(exWordsAndSentences) {


        var exWordsArr = exWordsAndSentences[0];
        var exSenArr = exWordsAndSentences[1];
        var resultArray = [result, exWordsArr, exSenArr];
        //succeed for firstPull
        succeed(resultArray);

      })
      
  })

})


}

//function for storing the results of a practice
let storePracticeResults = function(toStore, priority, timesPracticed) {

  var priority = priority;
  var newTimesPracticed = timesPracticed+1;

  return new Promise(function(succeed, fail)
  {

    MongoClient.connect(dbUrl, function(err, db) {
      if (err) throw err;
  
      var dbo = db.db("myAtlasDb");

      var ObjectId = require('mongodb').ObjectId;
      var myquery = {_id: ObjectId(toStore)};

      if (priority === "lowPriority") {

        var date = new Date();
        console.log(date);
        date.setDate(date.getDate() + 7)

        var newvalues = { $set: {lowPriority: true, medPriority: false, highPriority: false, nextRep: date, timesPracticed: newTimesPracticed} };
      }

      else if (priority === "medPriority") {
        var date = new Date();
        console.log(date);
        date.setDate(date.getDate() + 3)

        var newvalues = { $set: {lowPriority: false, medPriority: true, highPriority: false, nextRep: date, timesPracticed: newTimesPracticed} };
      }

      else {
        var date = new Date();
        console.log(date);
        date.setDate(date.getDate() + 1)

        var newvalues = { $set: {lowPriority: false, medPriority: false, highPriority: true, nextRep: date, timesPracticed: newTimesPracticed} };
      }
  
      dbo.collection("cards").updateOne(myquery, newvalues, function(err, result) {
        if (err) throw err;    

        db.close();
        succeed(result);
      });
  
    });    


  })

}
